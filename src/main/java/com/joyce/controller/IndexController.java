package com.joyce.controller;

import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.joyce.IndexApplication;
import com.joyce.service.IndexService;
/**
 * 这里的controller和service和其它微服务里的controller和service的所有方法名没有一个是重复的，这样方便从监控页面甄别。
 * 因为监控页面各个指标比较复杂，我还没彻底搞懂。
 * @author zhuwen
 *
 */
@RestController
public class IndexController {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(IndexController.class);
	
	@Value("${server.port}")
	private String serverPort;
	@Value("${spring.application.name}")
	private String spingApplicationName;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private IndexService indexService;

	@RequestMapping(value="/index")
	public String index(String name) {
		LOG.info("request /index param name=="+name);
		LOG.info("name==="+name);
		LOG.info("serverPort==="+serverPort);
		LOG.info("spingApplicationName==="+spingApplicationName);
		String result = "hello " + name+", " +spingApplicationName+":"+serverPort + ". ";
		return getCurrentTime() + result; //打上时间戳，方便观察结果
	}
	@RequestMapping(value="/index-UerSayHi")
	public String indexUerSayHi(String name) {
		LOG.info("request /index-UerSayHi param name=="+name);
		String result = "hello " + name+", " +spingApplicationName+":"+serverPort + ". ";
		result = result + indexService.getUserSayHi(name);
		return getCurrentTime() + result ;
	}
	@RequestMapping(value="/index-OrderName")
	public String indexOrderName(String name) {
		LOG.info("request /index-OrderName param name=="+name);
		String result = "hello " + name+", " +spingApplicationName+":"+serverPort + ". ";
		result = result + indexService.getOrderName(name);
		return getCurrentTime() + result ;
	}
	@RequestMapping(value="/testError")
	public String testError(String name) {
		LOG.info("request /testError param name=="+name);
		String result = "hello " + name+", " +spingApplicationName+":"+serverPort + ". ";
		result = result + restTemplate.getForObject("http://joyce-user/notExists?name="+name, String.class);;
		return getCurrentTime() + result ;
	}
	
	private String getCurrentTime() {
		return new Date().toLocaleString()+"   ";
	}
}
