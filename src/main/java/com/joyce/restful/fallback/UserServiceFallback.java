package com.joyce.restful.fallback;

import org.springframework.stereotype.Component;

import com.joyce.restful.UserService;

@Component
public class UserServiceFallback implements UserService{

	@Override
	public String userSayHi(String name) {
		return "hi " +name + ", joyce-user 发生异常！feign模式fallback.";
	}

}
