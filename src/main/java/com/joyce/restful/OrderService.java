package com.joyce.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.joyce.restful.fallback.UserServiceFallback;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class OrderService {
	
	@Autowired
	private RestTemplate restTemplate;

	@HystrixCommand(fallbackMethod="getOrderNameFallback")
	public String orderNameMethod(String name) {
		return restTemplate.getForObject("http://joyce-user/orderName?name="+name, String.class);
	};
	
	public String getOrderNameFallback(String name) {
		return "hi " +name + ", joyce-user 发生异常！@HystrixCommand模式fallback.";
	}
}
