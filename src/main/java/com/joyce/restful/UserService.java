package com.joyce.restful;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.joyce.restful.fallback.UserServiceFallback;

@FeignClient(value="joyce-user",fallback=UserServiceFallback.class)
public interface UserService {

	@RequestMapping(value="userSayHi")
	public String userSayHi(@RequestParam(value="name") String name);
}
