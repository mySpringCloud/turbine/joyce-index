package com.joyce.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.joyce.restful.OrderService;
import com.joyce.restful.UserService;

/**
 * @author zhuwen
 *
 */
@Service
public class IndexService {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private OrderService orderService;
	@Autowired
	private UserService userService;
	
	public String getOrderName(String name) {
		return orderService.orderNameMethod(name);
	}
	public String getUserSayHi(String name) {
		return userService.userSayHi(name);
	}
}
